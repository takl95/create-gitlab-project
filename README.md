
<h3 align="center">create-gitlab-project</h3>
  <p align="center">
   Create Gitlab projects simply from your CLI.
  </p>

<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>Table of Contents</summary>
  <ol>
<!--
    <li>
      <a href="#about-the-project">About The Project</a>
    </li>
-->
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>
</details>




[comment]: <> (## About The Project)


## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* Node

### Installation

  ```sh
  npm install create-gitlab-project -g
  ```


## Usage

* Global Installation:
  ```sh
  create-gitlab-project
  ```
* with NPX
  ```sh
  npx create-gitlab-project
  ```

## Contributing

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Merge Request / Pull Request



## License
Distributed under the MIT License. See `LICENSE.txt` for more information.



## Contact

Laszlo Takacs - laszlotakacs.95@gmail.com


