#!/usr/bin/env node

const {Command} = require('commander')
const chalk = require('chalk')
const request = require('request')
const getInput = require('node-piped').getInput

;(async () => {
  if (process.stdin.isTTY) {
    console.log(chalk.red('Token was not provided via pipe.'))
    process.exit()
  }

  let token = await getInput()
  token = token.replace('\n', '')
  const program = new Command()
  program
    .argument('<name>', 'Project name')
    .option(
      '-v, --visibility <visibility>',
      'Visibility of the project',
      'public'
    )
    .option(
      '-h, --host <host>',
      'Gitlab Provider Host. Default: gitlab.com',
      'gitlab.com'
    )
    .action((name, {visibility, host}) => {
      const requestOptions = {
        method: 'POST',
        url: `https://${host}/api/v4/projects`,
        headers: {
          'Content-Type':
            'multipart/form-data; boundary=---011000010111000001101001',
          Authorization: `Bearer ${token}`,
        },
        formData: {name, visibility},
      }

      request(requestOptions, (error, response, body) => {
        if (error) {
          throw new Error(error)
        }
        let data = JSON.parse(body)
        console.log(data.ssh_url_to_repo)
      })
    })

  program.parse()
})()
